import React from 'react';
import {ScrollView, TouchableWithoutFeedback, ViewProps} from 'react-native';
import {useTheme, variance} from '../../theming';
import {observer} from 'mobx-react-lite';
import {adaptiveView, Text, View} from '../../adaptive';
import {IconButton} from '../../HeaderView';
import Profile from '../../icons/Profile';
import Document from '../../icons/Document';
import MultipleDocument from '../../icons/MultipleDocument';
import People from '../../icons/People';
import {useRoot} from '../../Root';
import {formatPhoneNumber, fullName} from '../../helpers';
import {memberCountText} from '../../localization';
import {
  BlinkView,
  CircleView as Circle,
  FetchStatus,
  Icon,
  PaperIcon,
  PaperSecondaryText,
  PaperPrimaryText,
  PaperView,
  Grid,
} from '../../components';
import {BasicRequest} from '../../AsyncIO';
import Settings from '../../icons/Settings';
import Device from '../../icons/Device';
import Info from '../../icons/Info';
import CrossedDoc from '../../icons/CrossedDoc';

export interface ProfileScreenProps extends ViewProps {
  profileRequest: BasicRequest;
  goToPayment: () => void;
  goToPlanList: () => void;
  goToMemberList: () => void;
  goToSettings: () => void;
  goToSessionList: () => void;
  goToCollectiveAccessInfo: () => void;
  goToTerminateContract: () => void;
}

export default observer<ProfileScreenProps, React.ElementRef<typeof Root>>(
  (props, ref) => {
    const {
      profileRequest,
      goToPayment,
      goToPlanList,
      goToMemberList,
      goToSettings,
      goToSessionList,
      goToCollectiveAccessInfo,
      goToTerminateContract,
      ...rest
    } = props;
    const theme = useTheme();
    const root = useRoot();
    const isPending = profileRequest.isPending;
    const profile = root.readProfile.result;
    const surveillanceObject = root.readSurveillanceObject.result;
    const plan = root.readPlan.result;
    const memberList = root.readMemberList.result;
    const hasResult = !!profile && !!surveillanceObject && !!plan;
    const isCollectiveGuest = !profile?.owner && profile?.isCollective;
    const sessionList = root.readSessionList.result;

    return (
      <Root ref={ref} {...rest}>
        <Content
          refreshControl={
            <FetchStatus
              refreshing={hasResult && isPending}
              onRefresh={profileRequest.execute}
            />
          }>
          {!hasResult || isPending ? (
            <BlinkView>
              <PaperPlaceholder first />
              <PaperPlaceholder multiple />
              <PaperPlaceholder group />
              <PaperPlaceholder />
              <PaperPlaceholder />
              <PaperPlaceholder />
            </BlinkView>
          ) : profile ? (
            <>
              {surveillanceObject && !isCollectiveGuest && profile.id !== 3 ? (
                <Paper first>
                  <Grid>
                    <PaperSecondaryText>
                      Баланс лицевого счёта
                    </PaperSecondaryText>
                    <Balance>{surveillanceObject.balance.toFixed(2)} ₽</Balance>
                  </Grid>
                  <TouchableWithoutFeedback onPress={goToPayment}>
                    <TopUp>
                      <TopUpText>Пополнить</TopUpText>
                    </TopUp>
                  </TouchableWithoutFeedback>
                </Paper>
              ) : null}
              {surveillanceObject ? (
                <Paper
                  onPress={
                    profile.owner && profile.isCollective
                      ? goToCollectiveAccessInfo
                      : undefined
                  }>
                  <Grid grow>
                    <Grid row>
                      <Circle filled>
                        <PaperIcon>
                          <ProfileIcon />
                        </PaperIcon>
                      </Circle>
                      <Grid grow>
                        <PaperPrimaryText>{fullName(profile)}</PaperPrimaryText>
                        <PaperSecondaryText>
                          {formatPhoneNumber(profile.phoneNumber)}
                        </PaperSecondaryText>
                      </Grid>
                    </Grid>
                    <Divider />
                    <Grid row>
                      <Circle filled>
                        <IconButton>
                          <DocumentIcon />
                        </IconButton>
                      </Circle>
                      <Grid grow>
                        <PaperPrimaryText>
                          Договор №{surveillanceObject.contractId}
                        </PaperPrimaryText>
                        <PaperSecondaryText>
                          {surveillanceObject.address}
                        </PaperSecondaryText>
                      </Grid>
                      <Grid row>
                        {profile.owner && profile.isCollective && (
                          <InfoIcon width={24} color={theme.palette.primary} />
                        )}
                      </Grid>
                    </Grid>
                  </Grid>
                </Paper>
              ) : (
                <Paper
                  icon={<ProfileIcon />}
                  topText={fullName(profile)}
                  bottomText={formatPhoneNumber(profile.phoneNumber)}
                  onPress={goToPlanList}
                  chevron
                />
              )}
              {plan && !isCollectiveGuest ? (
                <Paper
                  icon={<MultipleDocumentIcon />}
                  topText="Управление тарифом"
                  bottomText={plan.name}
                  onPress={goToPlanList}
                  chevron
                />
              ) : null}
              {profile.owner && memberList ? (
                <Paper
                  icon={<PeopleIcon />}
                  topText="Управление доступом"
                  bottomText={memberCountText(memberList.length)}
                  onPress={goToMemberList}
                  chevron
                />
              ) : null}
              {profile.owner && sessionList?.length ? (
                <Paper
                  icon={<DeviceIcon />}
                  topText="Связанные устройства"
                  bottomText="Список авторизованных устройств"
                  onPress={goToSessionList}
                  chevron
                />
              ) : null}
              <Paper
                icon={<SettingsIcon />}
                topText="Настройки приложения"
                bottomText="Безопасность и внешний вид"
                onPress={goToSettings}
              />
              {profile.id === 3 ? (
                <Paper
                  icon={<CrossedDocIcon />}
                  topText="Закрыть договор"
                  onPress={goToTerminateContract}
                />
              ) : null}
            </>
          ) : null}
        </Content>
      </Root>
    );
  },
  {forwardRef: true},
);

const Root = variance(View)((theme) => ({
  root: {
    flex: 1,
    backgroundColor: theme.palette.backdrop,
  },
}));

const ProfileIcon = Icon(Profile);
const DocumentIcon = Icon(Document);
const MultipleDocumentIcon = Icon(MultipleDocument);
const PeopleIcon = Icon(People);
const SettingsIcon = Icon(Settings);
const DeviceIcon = Icon(Device);
const InfoIcon = Icon(Info);
const CrossedDocIcon = Icon(CrossedDoc);

export const Content = variance(adaptiveView(ScrollView))(
  () => ({
    root: {
      flex: 1,
    },
    container: {
      flexGrow: 1,
      paddingTop: 24,
      paddingBottom: 30,
    },
  }),
  (theme, _) => ({
    contentContainerStyle: _.container,
  }),
);

const Placeholder = variance(View)((theme) => ({
  root: {
    height: 24,
  },
}));

const Paper = variance(PaperView)(() => ({
  root: {
    marginTop: 8,
  },
  last: {
    marginBottom: 24,
  },
  group: {
    marginTop: 16,
  },
  first: {
    marginTop: 16,
  },
}));

const PaperPlaceholder = variance(View)((theme) => ({
  root: {
    minHeight: 78,
    marginTop: 8,
    marginHorizontal: 16,
    borderRadius: 12,
    padding: 18,
    paddingLeft: 16,
    backgroundColor: theme.palette.border,
  },
  first: {
    marginTop: 16,
  },
  last: {
    marginBottom: 24,
  },
  group: {
    marginTop: 16,
  },
  multiple: {
    minHeight: 138,
  },
}));

const Divider = variance(View)((theme) => ({
  root: {
    marginVertical: 13,
    backgroundColor: theme.palette.lift,
  },
}));

export const Balance = variance(Text)((theme) => ({
  root: {
    fontFamily: theme.fontByWeight[600],
    fontSize: 18,
    marginTop: 5,
    lineHeight: 18,
    color: theme.palette.textPrimary,
  },
}));

export const TopUp = variance(View)((theme) => ({
  root: {
    paddingTop: 14,
    paddingBottom: 12,
    paddingHorizontal: 16,
    height: 40,
    borderRadius: 20,
    backgroundColor: theme.palette.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export const TopUpText = variance(Text)((theme) => ({
  root: {
    fontFamily: theme.fontByWeight[600],
    fontSize: 14,
    lineHeight: 14,
    color: theme.contrast(theme.palette.primary),
  },
}));
