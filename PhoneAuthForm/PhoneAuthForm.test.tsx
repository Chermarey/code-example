import React from 'react';
import { renderWithProviders } from '../../../tests/renderWithProviders';
import PhoneAuthForm from './PhoneAuthForm';
import { render, screen } from '@testing-library/react';
import { changeInputMaskValue } from '../../../tests/changeInputValue';
import user from '@testing-library/user-event';
import Modal from 'react-modal';
import { RequestSubmitModal } from '../../../modals';
import delay from '../../../tests/delay';
import { requestCodeInitialState } from '../../../store/reducers/requestCode/requestCodeSlice';
import RequestCodeService from '../../../api/RequestCodeService';
import { AxiosResponse } from 'axios';
import AuthService from '../../../api/AuthService';

jest.unmock('react-modal');

const requestCodeFlow = async (phone: string) => {
  const { getPhoneInput, getSubmitButton, getSubmitModalButton } = getPhoneAuthTestEntries();
  changeInputMaskValue(getPhoneInput(), phone);
  user.click(getSubmitButton());
  await delay(100);
  user.click(getSubmitModalButton() ?? document.createElement('div'));
  await delay(100);
};

const sendCodeFlow = async (code: string, tryLogin: boolean) => {
  const { getSubmitButton, getCodeInput } = getPhoneAuthTestEntries();
  user.type(getCodeInput(), code);
  if (tryLogin) {
    user.click(getSubmitButton());
    await delay(100);
  }
};

const resendCodeFlow = async (tryLogin: boolean) => {
  const { getResendCodeButton, getSubmitButton } = getPhoneAuthTestEntries();
  await delay(30000);
  user.click(getResendCodeButton());
  await delay(100);
  if (tryLogin) {
    user.click(getSubmitButton());
    await delay(100);
  }
};

describe('PhoneAuthForm', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  it('Should render correctly', async () => {
    const result = await renderWithProviders(<PhoneAuthForm />, {});
    expect(result?.asFragment()).toMatchSnapshot();
  });

  it('Should not request code if phone is incorrect', async () => {
    const { getPhoneInput, getSubmitButton, getSubmitModalButton } = getPhoneAuthTestEntries();
    await renderPhoneAuthFormWithSubmitModal();

    changeInputMaskValue(getPhoneInput(), '7000000000');

    user.click(getSubmitButton());

    expect(getSubmitModalButton()).not.toBeInTheDocument();
  });

  it('Should login', async () => {
    mockPhoneAuthServicesResolvedValues();
    const result = await renderPhoneAuthFormWithSubmitModal();

    await requestCodeFlow('79513865540');

    await sendCodeFlow('1243', true);

    expect(result.store.getState().auth.isAuth).toEqual(true);
  });

  it('Should not requestCode if phone is correct but server sends error code', async () => {
    mockPhoneAuthServicesResolvedValues({ requestCodeError: true });
    const result = await renderPhoneAuthFormWithSubmitModal();

    await requestCodeFlow('79513865540');

    expect(result.store.getState().auth.isCodeSend).toEqual(false);
  });

  it('Should not login if code is wrong', async () => {
    mockPhoneAuthServicesResolvedValues({ sendCodeError: true });
    const result = await renderPhoneAuthFormWithSubmitModal();

    await requestCodeFlow('79513865540');

    await sendCodeFlow('1235', true);

    expect(result.store.getState().auth.isAuth).toEqual(false);
  });

  it('Should show timer to new request after code sent', async () => {
    mockPhoneAuthServicesResolvedValues();
    await renderPhoneAuthFormWithSubmitModal();
    const { getResendCodeTimer } = getPhoneAuthTestEntries();

    await requestCodeFlow('79513865540');

    await sendCodeFlow('1235', false);

    expect(getResendCodeTimer()).toBeInTheDocument();
  });

  it('Should resend after timeout passed and login if code is correct', async () => {
    mockPhoneAuthServicesResolvedValues();
    const result = await renderPhoneAuthFormWithSubmitModal();

    await requestCodeFlow('79513865540');

    await sendCodeFlow('1235', false);

    await resendCodeFlow(true);

    expect(result.store.getState().auth.isAuth).toEqual(true);
  });
});

const mockPhoneAuthServicesResolvedValues = ({
  requestCodeError,
  sendCodeError,
}: {
  requestCodeError?: boolean;
  sendCodeError?: boolean;
} = {}) => {
  const requestCodeServiceResponse = { session_token: 'token' };
  const sendCodeServiceResponse = { access_token: 'access', refresh_token: 'refresh' };
  if (requestCodeError) {
    jest.spyOn(RequestCodeService, 'requestCode').mockRejectedValue(new Error());
  } else {
    jest.spyOn(RequestCodeService, 'requestCode').mockResolvedValue({ data: requestCodeServiceResponse } as AxiosResponse);
  }
  if (sendCodeError) {
    jest.spyOn(AuthService, 'sendCode').mockRejectedValue(new Error());
  } else {
    jest.spyOn(AuthService, 'sendCode').mockResolvedValue({ data: sendCodeServiceResponse } as AxiosResponse);
  }
};

const renderPhoneAuthFormWithSubmitModal = (secondsToNewRequest?: number) => {
  render(<div data-testid='root' />);
  Modal.setAppElement(screen.getByTestId('root'));

  return renderWithProviders(
    <>
      <PhoneAuthForm />
      <RequestSubmitModal handleDestroy={jest.fn} handleHide={jest.fn} />
    </>,
    {
      requestCode: {
        ...requestCodeInitialState,
        secondsToNewRequest: secondsToNewRequest ?? requestCodeInitialState.secondsToNewRequest,
        requestCodeMethods: [
          { method: 'sms', desc: 'SMS' },
          { method: 'call', desc: 'call' },
        ],
      },
    },
  );
};

const getPhoneAuthTestEntries = () => ({
  getPhoneInput: () => screen.getByTestId('auth-phone-input') as HTMLInputElement,
  getSubmitButton: () => screen.getByTestId('auth-submit-button'),
  getSubmitModalButton: () => screen.queryByTestId('submit-modal-button'),
  getCodeInput: () => screen.getByTestId('auth-code-input'),
  getResendCodeTimer: () => screen.getByTestId('auth-resend-code-timer'),
  getResendCodeButton: () => screen.getByTestId('auth-resend-code-button'),
});
