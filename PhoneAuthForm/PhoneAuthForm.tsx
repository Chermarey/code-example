import React from 'react';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { useTypedSelector } from '../../../hooks/useTypedSelectors';
import { authActions } from '../../../store/reducers/auth/action-creators';
import { useRequestSubmitModal } from '../../../modals/RequestSubmitModal/RequestSubmitModal';
import { Formik } from 'formik';
import InputMask from 'react-input-mask';
import checkPhone from '../../../utils/checkPhone';
import { formatSeconds } from '../../../utils';
import refreshIcon from '../../../assets/img/svg/refresh.svg';

export const PhoneAuthSchema = Yup.object().shape({
  phone: Yup.string()
    .required('Введите номер телефона')
    .test({ test: phone => checkPhone(phone ?? ''), message: 'Введите валидный телефон' }),
});

const PhoneAuthForm: React.FC = () => {
  const dispatch = useDispatch();
  const isCodeSend = useTypedSelector(state => state.auth.isCodeSend);
  const requestCodeMethods = useTypedSelector(state => state.requestCode.requestCodeMethods);
  const currentRequestCodeMethodIndex = useTypedSelector(state => state.requestCode.currentRequestCodeMethodIndex);
  const currentRequestCodeMethod = requestCodeMethods[currentRequestCodeMethodIndex];
  const secondsToNewRequest = useTypedSelector(state => state.requestCode.secondsToNewRequest);
  const phone = useTypedSelector(state => state.auth.phoneNumber) ?? '';
  const { sendCode, requestCodeForAuth } = authActions;
  const { showModal: showRequestSubmitModal } = useRequestSubmitModal();

  const request = (phone: string) => {
    if (currentRequestCodeMethod && phone) {
      showRequestSubmitModal({
        onSubmit: () => dispatch(requestCodeForAuth({ phone })),
        text: currentRequestCodeMethod.desc,
      });
    }
  };

  return (
    <Formik
      initialValues={{ phone, code: '' }}
      validationSchema={PhoneAuthSchema}
      onSubmit={values => {
        if (!isCodeSend) {
          request(values.phone);
        } else {
          dispatch(sendCode({ code: values.code }));
        }
      }}
    >
      {({ values, handleChange, handleSubmit, errors }) => (
        <form onSubmit={handleSubmit}>
          {!isCodeSend ? (
            <div className='form-group pb-0 w-100 '>
              <label className={`label__title && ${errors.phone && 'warning'}`}>{errors.phone}</label>
              <InputMask mask={'+7 (999) 999-99-99'} value={values.phone} onChange={handleChange}>
                {() => (
                  <input
                    className={`input input-text-black ${errors.phone && 'input-bottom'}`}
                    name='phone'
                    data-testid='auth-phone-input'
                  />
                )}
              </InputMask>
            </div>
          ) : (
            <>
              <div className='authorization__codeInputField'>
                <input
                  className='input input-text-black'
                  value={values.code}
                  onChange={handleChange}
                  maxLength={4}
                  max={6}
                  placeholder='Введите код'
                  type='password'
                  name='code'
                  data-testid='auth-code-input'
                />
                <div className='authorization__resendCode'>
                  {secondsToNewRequest > 0 ? (
                    <div className='text-center' data-testid='auth-resend-code-timer'>
                      00:{formatSeconds(secondsToNewRequest)}
                    </div>
                  ) : (
                    <img
                      onClick={() => request(phone)}
                      src={refreshIcon}
                      alt='Отправить код повторно'
                      data-testid='auth-resend-code-button'
                    />
                  )}
                </div>
              </div>
            </>
          )}
          <button className='btn-blue mt-1 w-100' type='submit' data-testid='auth-submit-button'>
            {!isCodeSend ? 'Отправить код' : 'Войти'}
          </button>
        </form>
      )}
    </Formik>
  );
};

export default PhoneAuthForm;
