import { useDispatch } from 'react-redux';
import { authActions } from '../../../store/reducers/auth/action-creators';
import { Formik } from 'formik';
import React from 'react';

const ContractAuthForm: React.FC = () => {
  const dispatch = useDispatch();
  const { login } = authActions;

  return (
    <Formik
      initialValues={{ contract: '', password: '' }}
      onSubmit={({ contract, password }) => {
        dispatch(login({ password, contract }));
      }}
    >
      {({ values, handleChange, handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <input
            className='input input-text-black'
            value={values.contract}
            onChange={handleChange}
            name='contract'
            type='default'
            placeholder='Введите номер договора'
            data-testid='auth-contract-input'
          />
          <input
            className='input input-text-black'
            value={values.password}
            onChange={handleChange}
            type='password'
            name='password'
            placeholder='Введите пароль'
            data-testid='auth-password-input'
          />
          <button type='submit' className='btn-blue mt-1 w-100' data-testid='auth-submit-button'>
            Войти
          </button>
        </form>
      )}
    </Formik>
  );
};

export default ContractAuthForm;
