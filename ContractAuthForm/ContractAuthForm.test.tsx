import { renderWithProviders } from '../../../tests/renderWithProviders';
import React from 'react';
import ContractAuthForm from './ContractAuthForm';
import { screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import delay from '../../../tests/delay';
import { AxiosResponse } from 'axios';
import AuthService from '../../../api/AuthService';

const contractAuthFlow = async () => {
  const { getContractInput, getPasswordInput, getSubmitButton } = getContractAuthTestEntries();

  user.type(getContractInput(), '111111');

  user.type(getPasswordInput(), '123456');

  user.click(getSubmitButton());

  await delay(100);
};

describe('ContractAuthForm', () => {
  it('Should render correctly', async () => {
    const result = await renderWithProviders(<ContractAuthForm />, {});
    expect(result?.asFragment()).toMatchSnapshot();
  });

  it('Should login', async () => {
    const result = await renderWithProviders(<ContractAuthForm />, {});
    jest.spyOn(AuthService, 'login').mockResolvedValue({ data: { access_token: 'access', refresh_token: 'refresh' } } as AxiosResponse);

    await contractAuthFlow();

    expect(result?.store.getState().auth.isAuth).toEqual(true);
  });

  it('Should not login if server return that password or contract is wrong', async () => {
    const result = await renderWithProviders(<ContractAuthForm />, {});
    jest.spyOn(AuthService, 'login').mockRejectedValue(new Error());

    await contractAuthFlow();

    expect(result?.store.getState().auth.isAuth).toEqual(false);
  });
});

const getContractAuthTestEntries = () => ({
  getContractInput: () => screen.getByTestId('auth-contract-input'),
  getPasswordInput: () => screen.getByTestId('auth-password-input'),
  getSubmitButton: () => screen.getByTestId('auth-submit-button'),
});
