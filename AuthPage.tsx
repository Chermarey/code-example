import React, { useEffect, useState } from 'react';
import { authActions } from '../../store/reducers/auth/action-creators';
import { useDispatch } from 'react-redux';
import { fetchLogIn } from '../../store/reducers/screen/screenSlice';
import { PhoneAuthForm } from './PhoneAuthForm';
import { ContractAuthForm } from './ContractAuthForm';

const AuthPage: React.FC = () => {
  const dispatch = useDispatch();
  const { loginWithIp } = authActions;
  const [loginByPhone, setLoginByPhone] = useState(false);
  const title = `Войти по номеру ${!loginByPhone ? 'телефона' : 'договора'}`;

  useEffect(() => {
    (async function getUserIp() {
      const res = await fetch('', {
        method: 'GET',
      });
      const ip = Object.fromEntries(res.headers)['client-ip-addr'];
      if (ip) {
        dispatch(loginWithIp({ ip }));
      }
    })();
  }, []);

  useEffect(() => {
    dispatch(fetchLogIn());
  }, []);

  return (
    <div className='authorization'>
      <div className='authorization__container'>
        <div className='authorization__title'>{title}</div>
        {!loginByPhone ? <ContractAuthForm /> : <PhoneAuthForm />}
        <button className='authorization__type' onClick={() => setLoginByPhone(!loginByPhone)} data-testid='auth-toggle'>
          {title}
        </button>
      </div>
    </div>
  );
};

export default AuthPage;
