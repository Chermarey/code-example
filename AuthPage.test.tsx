import { renderWithProviders } from '../../tests/renderWithProviders';
import AuthPage from './AuthPage';
import React from 'react';
import { screen } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('AuthPage', () => {
  it('Should render correctly', async () => {
    const result = await renderWithProviders(<AuthPage />, {});
    expect(result?.asFragment()).toMatchSnapshot();
  });

  it('Should toggle auth', async () => {
    await renderWithProviders(<AuthPage />, {});
    const authToggle = screen.getByTestId('auth-toggle');

    user.click(authToggle);

    expect(screen.getByTestId('phone-auth-form')).toBeInTheDocument();

    user.click(authToggle);

    expect(screen.getByTestId('contract-auth-form')).toBeInTheDocument();
  });
});
